const keys = {

    privateKEY: process.env.prikey.replace(/\\n/g, '\n'),
    publicKEY: process.env.pubkey.replace(/\\n/g, '\n'),
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: 'code_temple'
};


module.exports = keys;