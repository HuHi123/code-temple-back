const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());

let UserController = require('../controllers/UserController');
app.use('/', UserController);
//professor
let ProjectController = require('../controllers/ProjectController');
app.use('/project', ProjectController);

let FileController = require('../controllers/FileController');
app.use('/file', FileController);

module.exports = app;