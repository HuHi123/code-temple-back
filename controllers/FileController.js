const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const File = require('../models/File');
const auth = require('../middleware/auth').authorize();
const radio = require('../radio');
//radio.emit('TreeUpdate',{projectId:null,data:null});
//radio.emit('EditorUpdate',{projectId:null,data:null});
router.use(bodyParser.json());
router.use(auth);

router.get('/project/:id',(req,res)=>{
    File.projectView(req.user.id,req.params.id,req.query.current)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/create',(req,res)=>{
    File.newFile(req.user.id,req.body.projectId,req.body.parentId,req.body.name,req.body.type)
        .then(result=>{
            res.status(200).json(result);
            radio.emit('TreeUpdate',{projectId:req.body.projectId,data:result});
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/delete',(req,res)=>{
    File.deleteFile(req.user.id,req.body.fid,req.body.type)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/changeName',(req,res)=>{
    File.changeName(req.user.id,req.body.fid,req.body.type,req.body.name)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});



router.post('/changeDefault',(req,res)=>{
    File.changeDefault(req.user.id,req.body.fid)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.get('/:id',(req,res)=>{
    File.getFile(req.user.id,req.params.id)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/edit',(req,res)=>{
    File.editFile(req.user.id,req.body.fid,req.body.content)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});




module.exports = router;

















