const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Project = require('../models/Project');
const auth = require('../middleware/auth').authorize();
const radio = require('../radio');

router.use(bodyParser.json());
router.use(auth);

router.get('/dashboard',(req,res)=>{
    Project.dashboard(req.user.id)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/create',(req,res)=>{
    Project.create(req.user.id,req.body.project)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/delete',(req,res)=>{
    Project.deleteProject(req.user.id,req.body.projectId)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/edit',(req,res)=>{
    Project.editProject(req.user.id,req.body.projectId,req.body.name)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/edit2',(req,res)=>{
    Project.editProject2(req.user.id,req.body.projectId,req.body.name)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/editParticipants',(req,res)=>{
    Project.editParticipants(req.user.id,req.body.participant,req.body.projectId)
        .then(result=>{
            res.status(200).json(result);

        })
        .catch(e=>{

            res.status(400).json(e.stack);
            console.log(e.message);
        })
});

router.post('/accept',(req,res)=>{
    Project.acceptProject(req.user.id,req.body.projectId)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/reject',(req,res)=>{
    Project.rejectProject(req.user.id,req.body.projectId)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});

router.post('/enter',(req,res)=>{
    Project.enterProject(req.user.id,req.body.projectId)
        .then(result=>{
            res.status(200).json(result);
        })
        .catch(e=>{
            res.status(400).json(e.message);
            console.log(e.message);
        })
});


module.exports = router;

