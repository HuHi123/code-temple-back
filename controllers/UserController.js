const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const User = require('../models/User');



router.post('/login',(req,res)=>{
    if(req.body.email && req.body.password)
        User.login(req.body.email,req.body.password)
            .then(result=>{
                res.status(200).json(result);
            })
            .catch(err=>{
                res.status(500).send(err.message);
            });
    else{
        res.status(400).send('ll');
    }

});

router.post('/signup',(req,res)=>{
    if(req.body.email && req.body.password && req.body.name)
        User.signup(req.body.name,req.body.email,req.body.password)
            .then(result=>{
                res.status(200).json(result);
            })
            .catch(err=>{
                res.status(500).end();
            });
    else{
        res.status(400).send('ww');
    }

});




module.exports = router;