'user strict';

class connitto {
    constructor(conn) {
        this.conz = conn;
        this.available = true;
    }

    async query(sql) {
        // if (!(this.available))
        //     return Promise.reject(new Error("Connection unavailable!!"));
        //var con = this.connection;
        return new Promise((resolve, reject) => {
            this.conz.query(sql, (err, rows) => {
                if (err) {
                    console.log("here");
                    return reject(err);
                }
                resolve(rows);
            });
        });
    }

    async query(sql,values) {
        // if (!(this.available))
        //     return Promise.reject(new Error("Connection unavailable!!"));
        //var con = this.connection;
        return new Promise((resolve, reject) => {
            this.conz.query(sql,values, (err, rows) => {
                if (err) {
                    console.log("here");
                    return reject(err);
                }
                resolve(rows);
            });
        });
    }

    async end() {
        if (this.available) {
            this.conz.end();
            this.available = false;
        }

    }

    async release() {
        if (this.available) {
            this.conz.release();
            this.available = false;
        }

    }

    //async beginTransaction

}
module.exports = connitto;

// //let conz;
// let available;
//
// module.exports = {
//     spawn: (conn) => {
//         return {
//             //     available: true,
//             //     connection: conn,
//
//             query: (sql) => {
//                 // if (!(this.available))
//                 //     return Promise.reject(new Error("Connection unavailable!!"));
//                 //var con = this.connection;
//                 return new Promise((resolve, reject) => {
//                     conz.query(sql, (err, rows) => {
//                         if (err) {
//                             console.log("here");
//                             return reject(err);
//                         }
//                         resolve(rows);
//                     });
//                 });
//             },
//             end: () => {
//                 if (available) {
//                     conz.end();
//                     available = false;
//                 }
//
//             },
//             release: () => {
//                 if (available) {
//                     conz.release();
//                     available = false;
//                 }
//             }
//         };
//     }
// };

