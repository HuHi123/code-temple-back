'user strict';

var mysql = require('mysql');
var Keys = require('../Keys.js');
const spawner =  require("./connection");

var pool  = mysql.createPool({
    connectionLimit : 1000,
    host     : Keys.host,
    user     : Keys.user,
    password : Keys.password,
    database : Keys.database,
    multipleStatements : false
});


pool.on('error', function(err) {
    console.log("[mysql error]",err);
});


pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

// module.exports = pool;

module.exports = {

    pool: pool,

    query: (sql) => {
        return new Promise((resolve, reject) => {
            pool.query(sql, (err, rows) => {
                if (err)
                    return reject(err);
                resolve(rows);
            });
        });
    },

    query: (sql,values) => {
        return new Promise((resolve, reject) => {
            pool.query(sql,values, (err, rows) => {
                if (err)
                    return reject(err);
                resolve(rows);
            });
        });
    },

    getConnection: () => {
        return new Promise((resolve, reject) => {
            pool.getConnection((err, conn) => {
                if (err)
                    return reject(err);
                resolve(new spawner(conn));
            });
        });
    },

};





/////////////notes



//use pool directly
/*
var mysql = require('mysql');
var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : 'datasoft123'
});

pool.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
  if (err) throw err;
  console.log('The solution is: ', rows[0].solution);
});
*/
//second approach
/*
var mysql = require('mysql');
var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : 'datasoft123',
  database        : 'hr'
});

pool.getConnection(function(err, connection) {
 // Use the connection
  connection.query( 'SELECT * from employees', function(err, rows) {
 // And done with the connection.
    console.log(rows[0]);
    connection.release();
 // Don't use the connection here, it has been returned to the pool.
  });
});
*/
