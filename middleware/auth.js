const Keys = require('../Keys.js');
const jwt = require('jsonwebtoken');
const publicKEY = Keys.publicKEY;

const verifyOptions = {
    expiresIn:  "100h",
    algorithm: ["RS256"]
};

function authorize() {

    return (req,res,next)=>{
        var re = new RegExp("Bearer .*");
        var token;
        const authorization = req.headers['authorization'];
        if(typeof authorization !== 'undefined' && re.test(authorization)) {
            token = authorization.substr(7, authorization.length);

            try {
                var decoded = jwt.verify(token, publicKEY, verifyOptions);
                req.user = decoded;
                next();

            } catch (e) {
                return res.status(401).send(JSON.stringify({status: "Unauthorized"}));//the token is expired or invalid
            }
        }else{
            res.status(400).send(JSON.stringify({status: "No token provided"}));//no or invalid authorization passed
        }
    }

}

function getId(token){
    try {
        let decoded = jwt.verify(token, publicKEY, verifyOptions);
        return decoded.id;

    } catch (e) {
        return false;
    }
}

function decode(token){
    try {
        return jwt.verify(token, publicKEY, verifyOptions);
    } catch (e) {
        return false;
    }
}

module.exports.authorize = authorize;
module.exports.getId = getId;
module.exports.decode = decode;