const pool = require('../database/db');
const radio = require('../radio');
const fs = require('fs');



let projectView = async (id,projectID,current,content)=>{
    if(current!==false)
        current = true;
    if(content===true)
        content=',content';
    else
        content='';
    return new Promise((resolve,reject)=>{
        let map = {};let parentMap = {};
        let data = {
            name:'',
            currentId : null,
            current: null,
            files: {
                name: '',
                id: 0,
                isDir: true,
                children: []
            },
            messages: [],
        };
        let promise;
        if(current)
            promise= pool.query('select File.content, User_Project.file, Project.name from User_Project' +
                ' inner join Project on Project.id=User_Project.pid ' +
                ' left join File on File.id = User_Project.file where uid=? and pid=?;',[id,projectID]);
        else
            promise= pool.query('select Project.name from User_Project' +
                ' inner join Project on Project.id=User_Project.pid ' +
                ' where uid=? and pid=?;',[id,projectID]);

        promise.then(res=>{
            if(res.length < 1)
                return Promise.reject(new Error('Not a participant!!'));
            if(res[0].file){
                data.currentId = res[0].file;
                data.current = res[0].content;
            }

            data.files.name = res[0].name;
        })
            .then(()=>{
                return Promise.all([
                    pool.query('select id, name, parent_id from Folder ' +
                        'where project_id=?;',[projectID])
                        .then(res=>{
                            res.forEach(el=>{
                                el.isDir = true;
                                el.children = [];
                                map[el.id]=el;
                                if(parentMap[el.id]){
                                    el.children=el.children.concat(parentMap[el.id]);
                                }
                                if(el.parent_id ===0 || el.parent_id ===null){
                                    data.files.children.push(el);
                                }else{
                                    if(map[el.parent_id]){
                                        map[el.parent_id].children.push(el);
                                    }else{
                                        if(!parentMap[el.parent_id])
                                            parentMap[el.parent_id]=[];
                                        parentMap[el.parent_id].push(el);
                                    }
                                }
                            })
                        }),
                    pool.query('select id, name, parent_id '+content+' from File ' +
                        'where project_id=?;',[ projectID])
                        .then(res=>{
                            res.forEach(el=>{
                                if(el.parent_id ===0 || el.parent_id ===null){
                                    data.files.children.push(el);
                                }else{
                                    if(map[el.parent_id]){
                                        map[el.parent_id].children.push(el);
                                    }else{
                                        if(!parentMap[el.parent_id])
                                            parentMap[el.parent_id]=[];
                                        parentMap[el.parent_id].push(el);
                                    }
                                }
                            })
                        }),
                    getMessages(id,projectID)
                        .then(res=>{
                            data.messages = res;
                        })

                ]);
            })
            .then(()=>{
                resolve(data);
            })
            .catch(err=>{
                reject(err);
            })
    })
};
//select Message.content as text, Message.date as date, Message.uid, User.name as title from User_Project inner join Message on Project.id = Message.pid  inner join User on User.id = Message.uid where User_Project.uid= ? and User_Project.pid=? ;
let getMessages = async (id,projectId)=>{
    return new Promise((resolve,reject)=>{
        pool.query('select Message.content as text, Message.date as date, Message.uid, User.name as title ' +
            'from User_Project inner join Message on User_Project.pid = Message.pid ' +
            ' inner join User on User.id = Message.uid ' +
            'where User_Project.uid= ? and User_Project.pid=? ;',[id,projectId])
            .then(res=>{
                res.forEach(el=>{
                    el.textColor='black';
                    el.type = 'text';
                    if(el.uid===id){
                        el.position = 'right';
                        el.title = 'You';
                    }
                    else
                        el.position = 'left';
                });
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })

    })
};
/*
{
   title: 'def',
   textColor:'black',
   position: 'right',
   type: 'text',
   text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
   date : new Date('1992-03-02 12:23:32')
}
*/

let sendMessage = async (id,projectId,text,date)=>{
    console.log(date);
    return new Promise((resolve,reject)=>{
        pool.query('select uid from User_Project where uid=? and pid=? ;',[id,projectId])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your project!!'));
                return pool.query('insert into Message (content,uid,pid,date)' +
                    ' values (?,?,?,?) ;',[text,id,projectId,date.slice(0, 19).replace('T', ' ')])
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })

    })
};

let createFiles = async (files,parent='')=>{

    return new Promise((resolve,reject)=>{
        const path = __dirname+'/../temps/folders/'+parent;
        if(files.isDir){
            fs.mkdir(path+'/'+files.name,(err)=>{
                if(err && err.code != 'EEXIST'){
                    console.log('error while creating folder '+files.name+': '+err.message);
                    reject(err);
                }
                else{
                    let arr =[];
                    files.children.forEach(el=>{
                        arr.push(createFiles(el,parent+'/'+files.name));
                    });
                    Promise.all(arr)
                        .then(()=>{
                            resolve();
                        })
                        .catch(err=>{
                            reject(err)
                        })
                }
            });
        }else{
            fs.writeFile(path+'/'+files.name,files.content,(err)=>{
                if(err){
                    console.log('error while creating file '+files.name+': '+err.message);
                    reject(err);
                }else{
                    resolve();
                }

            })
        }
    });


};



let fileExists = async (id,projectId,filename)=>{
  return new Promise((resolve,reject)=>{
      return pool.query('select * from User_Project inner join File on File.project_id = User_Project.pid ' +
          'where uid=? and pid=? and File.name=?;',[id,projectId,filename])
          .then(res=>{
              if(res.length < 1)
                  return Promise.reject(new Error('Error'));
              else
                  resolve(res);
          })
          .catch(err=>{
              reject(err);
          })
  })
};

let verifyProject = async (id,projectId)=>{
    return new Promise((resolve,reject)=>{
        return pool.query('select * from User_Project where uid=? and pid=?;',[id,projectId])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your project!'));
                else
                    resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let verifyFile = async (id,fileId)=>{
    return new Promise((resolve,reject)=>{
        return pool.query('select * from User_Project inner join File on File.project_id=User_Project.pid where uid=? and File.id=?;',[id,fileId])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your project!'));
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let newFile = async (userId,projectId,parentId,name,type)=>{
    return new Promise((resolve,reject)=>{
        if (type!==2 && type!==1) {
            reject(new Error('Invalid option!!'));
        }
        return pool.query('select Folder.id,User_Project.pid from User_Project left join Folder on User_Project.pid = Folder.project_id  ' +
            'where User_Project.pid=? and User_Project.uid=?;',[projectId,userId,parentId])
            .then(res=>{
                if(res.length<1)
                    return Promise.reject(new Error('Not your Project!!'));
                if(parentId!==0 && res[0].id==null)
                    return Promise.reject(new Error('Chosen folder doesn\'t belong to project!!'));
                if(parentId===0){
                    parentId=null;
                }else{
                    let f =false;
                    res.forEach(el=>{
                        if(el.id==parentId)
                            f=true;
                    });
                    if(!f)
                        return Promise.reject(new Error('Chosen folder doesn\'t belong to project!!'));
                }

                if(type===1){
                    console.log('sds'+name+parentId+projectId);
                    return pool.query('insert into File (name,parent_id,project_id,content) values (?,?,?,?);',[name,parentId,projectId,'']);
                }
                if(type===2)
                    return pool.query('insert into Folder (name,parent_id,project_id) values (?,?,?);',[name,parentId,projectId]);
            })
            .then(res=>{
                return projectView(userId,projectId,false)
            })
            .then(res=>{
                radio.emit('TreeUpdate',{projectId:projectId,data:res});
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let deleteFile = async (id,fid,type)=>{
    return new Promise((resolve,reject)=>{
        let p;
        if(type!==1 && type!==2)
            reject(new Error('Invalid option!!!'));
        if(type===1)
            type='File';
        else
            type='Folder';
        return pool.query('select * from ?? inner join User_Project on ??.project_id = User_Project.pid where ??.id = ? and User_Project.uid=?;',[type,type,type,fid,id])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your file!!!'));
                p=res[0].project_id;
                return pool.query('delete from ?? where id=?',[type,fid]);
            })
            .then(res=>{
                return projectView(id,p,false)
            })
            .then(res=>{
                radio.emit('TreeUpdate',{projectId:p,data:res});
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    });
};

let changeName = async (id,fid,type,name)=>{//req.user.id,req.body.fid,req.body.type,req.body.name
    return new Promise((resolve,reject)=>{
        let p;
        if(type!==1 && type!==2)
            reject(new Error('Invalid option!!!'));
        if(type===1)
            type='File';
        else
            type='Folder';
        return pool.query('select * from ?? inner join User_Project on ??.project_id = User_Project.pid where ??.id = ? and User_Project.uid=?;',[type,type,type,fid,id])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your file!!!'));
                p=res[0].project_id;
                return pool.query('update ?? set name =? where id=?;',[type,name,fid]);
            })
            .then(res=>{
                return projectView(id,p,false)
            })
            .then(res=>{
                radio.emit('TreeUpdate',{projectId:p,data:res});
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    });
};

let changeDefault = async (id,fileId)=>{
    return new Promise((resolve,reject)=>{
        return pool.query('select User_Project.pid from File inner join User_Project on User_Project.pid = File.project_id ' +
            'where User_Project.uid=? and File.id=?;',[id,fileId])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your file!!'));
                return pool.query('update User_Project set file=? where uid=? and pid=?;',[id,res[0].pid])
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let getFile = async (id,fileId)=>{
    return new Promise((resolve,reject)=>{
        return pool.query('select File.content from File inner join User_Project on User_Project.pid = File.project_id ' +
            'where User_Project.uid=? and File.id=?;',[id,fileId])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your file!!'));
                resolve(res[0].content);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let editFile = async (id,fileId,content)=>{
    return new Promise((resolve,reject)=>{
        return pool.query('select File.id from File inner join User_Project on User_Project.pid = File.project_id ' +
            'where User_Project.uid=? and File.id=?;',[id,fileId])
            .then(res=>{
                if(res.length < 1)
                    return Promise.reject(new Error('Not your file!!'));
                return pool.query('update File set content=? where id=?;',[content,fileId]);
            })
            .then(res=>{
                //radio.emit('EditorUpdate',{projectId:null,data:null});
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let editFile2 = async (fileId,content)=>{
    return new Promise((resolve,reject)=>{

        return pool.query('update File set content=? where id=?;',[content,fileId])
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};


module.exports={
    projectView: projectView,
    newFile:newFile,
    deleteFile:deleteFile,
    changeName:changeName,
    changeDefault:changeDefault,
    getFile:getFile,
    editFile:editFile,
    verifyProject:verifyProject,
    verifyFile:verifyFile,
    editFile2:editFile2,
    createFiles:createFiles,
    getMessages:getMessages,
    sendMessage:sendMessage,
    fileExists:fileExists,

};



// projectView(userId,projectId)
//     .then(res=>{
//         radio.emit('TreeUpdate',{projectId:projectId,data:res});
//     }).catch(()=>{});








