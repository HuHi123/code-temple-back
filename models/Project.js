const pool = require('../database/db');
const radio = require('../radio');
const file = require('./File');



let dashboard = async (id)=>{
    let projects=[];let res={name:'',projects:projects,requests:[]};
    return new Promise((resolve,reject)=>{
        pool.query('select id,name from User where id =?;',[id])
            .then(resp=>{
                if(resp<1)
                    return Promise.reject(new Error('Not a User!!'));
                res.name = resp[0].name;
                return pool.query('select k.id,k.name,User.name as username,User.email ' +
                    'from (select Project.id,Project.name from Project inner join User_Project on User_Project.pid = Project.id where User_Project.uid = ?) as k ' +
                    'inner join User_Project on k.id = User_Project.pid ' +
                    'inner join User on User_Project.uid = User.id;',[id]);
            })
            .then(result=>{
                let map ={};
                result.forEach(el=>{
                    if(map[el['id']]){
                        map[el['id']].participants.push({name:el.username,email:el.email});
                    }else{
                        map[el['id']] = {
                            name: el.name,
                            id: el.id,
                            participants: [{name:el.username,email:el.email}]
                        };
                        projects.push(map[el['id']]);
                    }
                });
                return pool.query('select Project.id as id, User.name as user, User.email as email, Project.name as name from ' +
                    'Invite inner join Project on Project.id = Invite.pid inner join User on Invite.uid = User.id ' +
                    'where Invite.invited =?;',[id])
            })
            .then(result=>{
                res.requests = result;
                console.log('[][][]]');
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    });
};

let create= async (id,project)=>{
    return new Promise((resolve,reject)=>{
        let con;
        pool.getConnection()
            .then(conn=>{
                con=conn;
                return con.query("SET autocommit= 0 ;");
            })
            .then(()=>{
                return con.query('insert into Project (name) values (?) ;',[project]);
            })
            .then(res=>{
                return con.query('insert into User_Project (uid,pid) values (?,?);',[id,res.insertId]);
            })
            .then(()=>{
                return Promise.all([
                    con.query('COMMIT;'),
                    con.query('SET autocommit= 1 ;')
                ]);

            })
            .then(()=>{
                con.release();
                return dashboard(id)
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
                con.release();
            })
    });
};


let deleteProject = async (id,projectId)=>{
    return new Promise((resolve,reject)=>{
        let con;
        pool.getConnection()
            .then(conn=>{
                con=conn;
                return con.query("SET autocommit= 0 ;");
            })
            .then(()=>{
                return con.query('delete from User_Project where pid=? and uid=? ;',[projectId,id])
            })
            .then(res=>{
                return con.query('select uid from User_Project where pid=?;',[projectId]);
            })
            .then(res=>{
                if(res.length>0){
                    console.log('dee');
                    res.forEach(el=>{

                        radio.emit('update',el.uid);
                    });
                    return Promise.reject();
                }
                return con.query('select Invited from Invite where pid=?;',[projectId]);
            })
            .then(res=>{
                if(res.length>0) {
                    console.log('ppp');
                    return Promise.reject();

                }
                return con.query('delete from Project where id=? ;',[projectId])
            })
            .then(()=>{
                return Promise.all([
                    con.query('COMMIT;'),
                    con.query('SET autocommit= 1 ;')
                ])
            })
            .catch(()=>{
                return Promise.all([
                    con.query('COMMIT;'),
                    con.query('SET autocommit= 1 ;')
                ])
            })
            .then(()=>{
                con.release();
                return dashboard(id)
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                con.release();
                reject(err);
            })
    });
};

let editProject = async (id,projectId,name)=>{
    return new Promise((resolve,reject)=>{
        pool.query('select * from User_Project where pid=? and uid=?;',[projectId,id])
            .then(res=>{
                if(res.length<1)
                    return Promise.reject(new Error('Not your project!!'));
                return pool.query('update Project set name=? where id=? ;',[name,projectId]);
            })
            .then(res=>{
                pool.query('select uid from User_Project where pid=? and uid!=?;',[projectId,id])
                    .then(r=>{
                        r.forEach(el=>{
                            console.log(el.uid);
                            radio.emit('update',el.uid);
                        });
                    }).catch(()=>{});
                pool.query('select invited from Invite where pid=?;',[projectId])
                    .then(r=>{
                        r.forEach(el=>{
                            console.log(el.invited);
                            radio.emit('update',el.invited);
                        });
                    }).catch(()=>{});
                // file.projectView(id,projectId,false)
                //     .then(res=>{
                //         radio.emit('TreeUpdate',{projectId:projectId,data:res});
                //     }).catch(()=>{})

                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let editProject2 = async (id,projectId,name)=>{
    let p = projectId;
    return new Promise((resolve,reject)=>{
        editProject(id,projectId,name)
            .then(()=>{
                return file.projectView(id,p,false)
            })
            .then(res=>{
                // radio.emit('TreeUpdate',{projectId:projectId,data:res});
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })
    })
};

let editParticipants = async (id,participant,projectId)=>{
    let con;let sid;
    return new Promise((resolve,reject)=>{
        pool.getConnection()
            .then(conn=>{
                con=conn;
                return con.query("SET autocommit= 0 ;");
            })
            .then(()=>{
                return con.query('select * from User_Project where pid=? and uid=?;',[projectId,id])
                    .then(res=>{
                        if(res.length<1)
                            return Promise.reject(new Error('Not your project!!'));
                    })
            })
            .then(()=>{
                return con.query('select id from User where email=?;',[participant])
            })
            .then(res=>{
                if(res.length<1 || res[0].id===id)
                    return Promise.reject(new Error('Cannot add participant!!'));
                sid = res[0].id;
                return con.query('select * from User_Project where uid =? and pid=?;',[sid,projectId])
            })
            .then(res=>{
                if(res.length>0){
                    console.log('oooo');
                    return Promise.reject(new Error('Already a participant'));
                }

                return con.query('insert into Invite (uid,invited,pid) values (?,?,?);',[id,sid,projectId])
            })
            .then(()=>{
                return Promise.all([
                    con.query('COMMIT;'),
                    con.query('SET autocommit= 1 ;')
                ]);

            })
            .then(res=>{
                con.release();
                radio.emit('update',sid);
                resolve(res);
            })
            .catch(err=>{
                con.release();
                reject(err);
            })

    })
};

let acceptProject = async (id,projectId)=>{//
    let con;
    return new Promise((resolve,reject)=>{
        pool.getConnection()
            .then(conn=>{
                con=conn;
                return con.query("SET autocommit= 0 ;");
            })
            .then(()=>{
                return con.query('select uid from Invite where invited=? and pid=?;',[id,projectId]);
            })
            .then(res=>{
                if(res.length<1)
                    return Promise.reject(new Error('You are not invited!!'));
                else return res;
            })
            .then(res=>{
                return con.query('insert into User_Project (uid,pid) values (?,?);',[id,projectId]);
            })
            .then(res=>{
                return con.query('delete from Invite where invited=? and pid=? ;',[id,projectId]);
            })
            .then(()=>{
                return Promise.all([
                    con.query('COMMIT;'),
                    con.query('SET autocommit= 1 ;')
                ]);
            })
            .then(()=>{
                con.release();
                return dashboard(id)
            })
            .then(res=>{
                pool.query('select uid from User_Project where pid=? and uid!=?;',[projectId,id])
                    .then(res=>{
                        res.forEach(el=>{
                            radio.emit('update',el.uid);
                        });
                    });

                resolve(res);
            })
            .catch(err=>{
                con.release();
                reject(err);
            })

    })
};

let rejectProject = async (id,projectId)=>{
    return new Promise((resolve,reject)=>{
        pool.query('delete from Invite where invited=? and pid=? ;',[id,projectId])
            .then(()=>{
                return pool.query('select uid from User_Project where pid=?;',[projectId])
            })
            .then(res=>{
                if(res.length<1)
                    return pool.query('delete from Project where id=? ;',[projectId]);
            })
            .then(()=>{
                return dashboard(id)
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })

    })
};

let enterProject = async (id,projectId)=>{
    return new Promise((resolve,reject)=>{
        pool.query('delete from Invite where invited=? and pid=? ;',[id,projectId])
            .then(()=>{
                return dashboard(id)
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })

    })
};

let getMessages = async (id,projectId)=>{
    return new Promise((resolve,reject)=>{
        pool.query('select * from User_Project inner join Message on Project.id = Message.pid ' +
            'where User_Project.uid= ? and User_Project.pid=? ;',[id,projectId])
            .then(()=>{
                return dashboard(id)
            })
            .then(res=>{
                resolve(res);
            })
            .catch(err=>{
                reject(err);
            })

    })
};

module.exports = {
    enterProject:enterProject,
    rejectProject:rejectProject,
    acceptProject:acceptProject,
    dashboard: dashboard,
    create: create,
    deleteProject:deleteProject,
    editProject:editProject,
    editParticipants:editParticipants,
    editProject2:editProject2,
};
