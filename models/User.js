let pool = require('../database/db');
let utils = require('../utils');
let jwt = require('jsonwebtoken');

module.exports ={

    login: async (email,password)=>{
        return new Promise((resolve,reject)=>{
            pool.query("select * from User where email=?;",[email])
                .then(res=>{
                    if(res.length !== 1)
                        return Promise.reject(new Error('No such user'));
                    if(utils.hash(password,res[0].salt)!==res[0].password)
                        return Promise.reject(new Error('Incorrect password'));


                    resolve({
                        token : utils.createJwt({
                            id: res[0].id,
                            name: res[0].name,
                            email: res[0].email
                        })
                    });
                })
                .catch((err)=>{
                    reject(err);
                })
        })
    },

    signup: async (name,email,password)=>{
        return new Promise((resolve,reject)=>{
            let tmp = utils.saltedHash(password);
            let hash = tmp.hash;
            let salt = tmp.salt;
            pool.query("insert into User (name,email,password,salt) values (?,?,?,?);",[name,email,hash,salt])
                .then(res=>{

                    resolve({
                        token : utils.createJwt({
                            id: res.insertId,
                            name: name,
                            email: email
                        })
                    });
                })
                .catch((err)=>{
                    reject(err);
                })
        })
    }


};