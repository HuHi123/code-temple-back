const socket =  require('socket.io');
const getId = require('./middleware/auth').getId;
const decode = require('./middleware/auth').decode;
const getDashboard= require('./models/Project').dashboard;
let verify =  require('./models/File').verifyProject;
let verifyFile =  require('./models/File').verifyFile;
let editFile =  require('./models/File').editFile2;
let addMessage =  require('./models/File').sendMessage;
let runner = require('./runner');

const radio = require('./radio');

module.exports = function listen(server){

    const io = socket(server);
    //Stuff related to the dashboard page
    const DashboardSpace =  io.of('/dashboard');

    radio.on('update',(id)=>{
        getDashboard(id)
            .then(res=>{
                DashboardSpace.to(id).emit('update',res)
            });
    });


    DashboardSpace.on('connection', function (socket) {
        console.log('Client connected to dashboard namespace!');
        socket.on('join',(data)=>{
            let token = data.token;
            let room = getId(token);
            if(room!==false){
                socket.join(room);
                socket.on('update',()=>{
                    getDashboard(room)
                        .then(res=>{
                            socket.to(room).broadcast.emit('update',res)
                        });

                })
            }

        });
        socket.on('disconnect',()=>{
            console.log('unit lost of dashboard namespace!!');
        });

    });

    //Stuff related to the tree
    const TreeSpace = io.of('/tree');

    radio.on('TreeUpdate',(res)=>{
        TreeSpace.to(res.projectId).emit('TreeUpdate',res.data)
    });

    TreeSpace.on('connection', function (socket) {
        console.log('Client connected to tree namespace!');
        let proc = null;
        socket.on('join',(data)=>{
            let token = data.token;
            let id = getId(token);

            if(id!==false){
                verify(id,data.projectId)
                    .then(()=>{
                        socket.join(data.projectId);
                    })
            }

        });

        socket.on('run',(data)=>{
            let token = data.token;
            let id = getId(token);

            if(id!==false){
                verify(id,data.projectId)
                    .then(async ()=>{
                        if(proc!==null){
                            proc.terminate();
                        }
                        console.log('oooooo');
                        let p;
                        try {
                            p = await runner.containedRun(id,data.projectId);//run
                        }catch{return Promise.reject()}
                        p.on('data',res=>{
                            socket.emit('stack',''+res);
                            console.log('ioio: '+res);
                        });
                        p.on('close',()=>{
                            proc=null;
                            console.log('colseddddd');
                            socket.emit('termination','ss');
                        });
                        proc=p;
                    })
                    .catch(()=>{
                        socket.emit('termination','ss');
                    })
            }

        });

        socket.on('stop',()=>{
            if(proc!==null){
                proc.terminate();
            }
        });

        socket.on('message',(token,message)=>{

            let decoded = decode(token);
            let id = decoded.id;
            let name = decoded.name;

            addMessage(id,message.projectId,message.text,message.date)
                .then(()=>{
                    socket.to(message.projectId).broadcast.emit('message',{
                        ...message,
                        textColor:'black',
                        position: 'left',
                        type: 'text',
                        title: name,
                    })
                }).catch(err=>{ console.log('Couldn\'t add message!! '+err.message); })
        });

        socket.on('disconnect',()=>{
            if(proc!==null){
                proc.terminate();
            }
            console.log('unit lost of tree namespace!!');
        });



        socket.on('write',data=>{
            if(proc!==null){
                console.log(data);
                proc.write(data);
            }
        });

    });




    //Stuff related to the file
    const FileSpace = io.of('/file');

    FileSpace.on('connection', function (socket) {
        console.log('Client connected to file namespace!');
        socket.on('join',(data)=>{
            let token = data.token;
            let id = getId(token);
            if(id!==false){

                verifyFile(id,data.fileId)
                    .then(()=>{
                        console.log('mkmk'+id+' dfd '+data.fileId);
                        socket.join(data.fileId);
                        socket.on('FileUpdate',(content)=>{
                            editFile(data.fileId,content)
                                .then(()=>{
                                    socket.to(data.fileId).broadcast.emit('FileUpdate',content)
                                }).catch(err=>{console.log('error!!'+err)})
                        })
                    })
            }

        });

        socket.on('disconnect',()=>{
            console.log('unit lost of file namespace!!');
        });

    });

};



