const exec = require('child_process').exec;
const spawn = require('child_process').spawn;
const event = require('events');
const fileModel = require('./models/File');
const fs = require('fs');

let run = async (id,projectId)=>{

    let projectRoot = await createFiles(id,projectId);
    return new MyProcess('node',[__dirname+'/temps/folders/'+projectRoot+'/server.js']);
};

let containedRun = async (id,projectId)=>{
    return new containedProcess(id,projectId);
};

let justRun = async (cmd,args)=>{
    return new Promise((resolve,reject)=>{
        let checkProcess = spawn(cmd,args);
        checkProcess.on('close', (code, signal)=> {
            if(code!=0){
                reject('err');
            }
            else
                resolve('OK');
        });
        checkProcess.on('error', (code, signal)=> {

            checkProcess.kill('SIGKILL');
            reject('err');
        });
    });
};

let dockerImageExists = async (tag)=>{
    return new Promise((resolve,reject)=> {
        return justRun('docker', ['image', 'inspect', tag])
            .then(res=>{resolve(res)})
            .catch(err=>{reject(err)})
    })

};

let createContainer = async(name,image,cmd)=>{
    if(!cmd)
        cmd=[];
    return new Promise((resolve,reject)=> {
        return justRun('docker', ['create','-p', '4000:4000','--name', name, image].concat(cmd))
            .then(res=>{resolve(res)})
            .catch(err=>{reject(err)})
    })
};

let copyFilesToContainer = async(directory,containerName)=>{
    return new Promise((resolve,reject)=> {
        console.log('docker container cp '+directory+' '+containerName+':/usr/src/app/');
        return justRun('docker', ['container', 'cp', directory+'/.', containerName+':/usr/src/app/'])
            .then(res=>{resolve(res)})
            .catch(err=>{reject(err)})
    })
};



class containedProcess extends event{
    //events emitted: data close
    constructor(id,projectId){
        super();
        this.rootFolder = '';
        this.containerProcess=null;
        this.flag = true;
        this.first = true;
        let wholeThing = fileModel.fileExists(id,projectId,'server.js')
            .then(()=>{
                this.emit('data','Compiling...');
                if(!this.flag)
                    return Promise.reject('err');
                return dockerImageExists('node_runner')
            })
            .then(()=>{
                console.log('f2');
                if(!this.flag)
                    return Promise.reject('err');
                return createFiles(id,projectId)
            })
            .then((rootFolder)=>{
                console.log('f3');
                if(!this.flag)
                    return Promise.reject('err');
                this.rootFolder= rootFolder;
                return fileModel.fileExists(id,projectId,'package.json')
                    .then(()=>{
                        console.log('yes');
                        return createContainer(this.rootFolder,'node_runner')
                    })
                    .catch(()=>{
                        return createContainer(this.rootFolder,'node_runner',['node','server.js'])//'node','server.js'
                    })
            })
            .then(()=>{
                console.log('f4');
                if(!this.flag)
                    return Promise.reject('err');
                return copyFilesToContainer(__dirname+'/temps/folders/'+this.rootFolder,this.rootFolder);
            })
            .then(()=>{
                console.log('f5');
                if(!this.flag)
                    return Promise.reject('err');
                this.containerProcess = spawn('docker',['container','start','-a',''+this.rootFolder]);
                this.containerProcess.on('close', (code, signal)=> {
                    this.emit('close');
                    console.log('finished boy!!!!!!!!!');
                    cleanFile(__dirname+'/temps/folders/'+this.rootFolder);
                    cleanContainer(this.rootFolder)
                });
                this.containerProcess.on('error', (code, signal)=> {
                    this.containerProcess.kill('SIGKILL');
                    this.emit('close');
                    console.log('finished boy!!!!!!!!!');
                    cleanFile(__dirname+'/temps/folders/'+this.rootFolder);
                    cleanContainer(this.rootFolder)
                });
                this.containerProcess.stdout.on('data', (data)=> {
                    //process.stdout.write(data);
                    if(this.flag){
                        if(this.first){
                            this.emit('data','Finished compiling...\n............');
                            this.first=false;
                        }
                        this.emit('data',data);
                    }

                });
                this.containerProcess.stderr.on('data', (data)=> {
                    //process.stderr.write(data);

                    if(this.flag){
                        if(this.first){
                            this.emit('data','Finished compiling...');
                            this.first=false;
                        }
                        this.emit('data',data);
                    }

                });
                this.containerProcess.stdin.setEncoding('utf-8');
            })
            .catch(()=>{

                this.emit('close');
                console.log('finished boyx!!!!!!!!!');
                cleanFile(__dirname+'/temps/folders/'+this.rootFolder);
                cleanContainer(this.rootFolder)
                //return Promise.reject('err');
            })

    }

    terminate(){
        if(this.containerProcess)
            this.containerProcess.kill('SIGKILL');
        this.flag = false;
        this.emit('close');
    }
    write(input){
        this.proc.stdin.write(input);
    }

}

let createFiles = async (id,projectId)=>{
    let randString = ('' + Math.random()).substr(8);
    try {
        let res = await fileModel.projectView(id, projectId, false, true);
        res.files.name = (res.files.name + '' + id + randString).replace(' ','');
        await fileModel.createFiles(res.files);
        return res.files.name;
    }catch{
        return false;
    }

};

let cleanFile = (rootFile)=>{
    fs.rmdir(rootFile,{recursive:true}, (err)=>{
        if(err)
            console.log('Failed to remove file :( '+rootFile)
    })
};

let cleanContainer = (containerName)=>{
    let container = spawn('docker',['container','rm','-f',containerName ]);
    container.on('close', (code, signal)=> {
        console.log('closed!');
    });
    container.on('error', (code, signal)=> {
        console.log('Failed to remove container :( '+containerName);
    });
};


class MyProcess extends event{
    constructor(cmd,args){
        super();
        this.proc = spawn(cmd,args);
        this.proc.stdout.on('data', (data)=> {
            //process.stdout.write(data);
            this.emit('data',data);
        });
        this.proc.stderr.on('data', (data)=> {
            //process.stderr.write(data);
            this.emit('data',data);
        });
        this.proc.on('close', (code, signal)=> {
            this.emit('close');
            this.proc.kill('SIGKILL');
            cleanFile(args[0]);
        });
        this.proc.on('error', (code, signal)=> {
            this.emit('close');
            this.proc.kill('SIGKILL');
            cleanFile(args[0]);
        });
        this.proc.stdin.setEncoding('utf-8');

    }

    terminate(){
        this.proc.kill('SIGKILL');
    }
    write(input){
        this.proc.stdin.write(input);
    }
}



module.exports={
    run:run,
    // run2:run2,
    containedRun:containedRun,
    createFiles:createFiles,
};