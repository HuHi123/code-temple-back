const app = require('./apps/app');
const server = require('http').Server(app);
require('./pusher')(server);



server.listen(8000, () => {
    console.log(`Express running → PORT ${server.address().port}!!`);
});
