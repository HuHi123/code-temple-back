'use strict';
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const keys = require('./Keys');

let saltSize = 16; /** 16 byte salt */
let algo = 'sha512'; /** Hashing algorithm sha512 */
let signOptions = { /** sign options for the jwt token*/
        algorithm: 'RS256',
        expiresIn: '100h'
    };

let genSalt = function (length) {
        return crypto.randomBytes(Math.ceil(length / 2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0, length);   /** return required number of characters */
    };


let hash = function (password, salt) {
    let hash = crypto.createHmac(algo, salt);
    hash.update(password);
    return hash.digest('hex');
};


module.exports = {

    saltedHash: (userpassword) => {
        let salt = genSalt(saltSize);
        return {hash:hash(userpassword, salt),salt:salt};
    },

    hash: hash,

    createJwt : (payload)=>{
        return jwt.sign(payload, keys.privateKEY, signOptions)
    }

};
